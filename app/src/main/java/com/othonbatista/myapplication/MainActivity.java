package com.othonbatista.myapplication;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Chamada do metodo onCreate da classe pai
        super.onCreate(savedInstanceState);

        // Atribui a variavel de instancia ContentView a tela do app (activity_main)
        setContentView(R.layout.activity_main);

        // Declara um objeto da classe Button
        // Liga o objeto declarado ao que esta na tela com o id button
        Button botao = (Button) findViewById(R.id.button);
/*
        // Esta e uma das maneiras de criar um tratador de eventos para o clique
        // em botao.
        // Declara-se um objetto da classe OnClickListener e cria-se o
        // objeto para implementar o metodo onClick

        View.OnClickListener botaoClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Cria um dialogo para exibir na tela
                AlertDialog.Builder dialogo = new AlertDialog.Builder(MainActivity.this);

                // Atribui as propriedades do dialogo
                dialogo.setTitle("Diálogo");
                dialogo.setMessage("Apresentando um Diálogo com uma mensagem simples!!!");
                dialogo.setNeutralButton("OK", null);

                // Apresenta o dialogo na tela
                dialogo.show();
            }
        };

        // Atribui a propriedade OnClickListener de botao o objeto botaoClick
        botao.setOnClickListener (botaoClick);
*/
/*
         Outra maneira de criar um tratador de eventos para o clique em
        botao ja atribuindo a propriedade OnClickListener a uma instancia
        da classe OnClickListener sem a criacao de uma variavel
*/
        botao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              // Cria um dialogo para exibir na tela
              AlertDialog.Builder dialogo = new AlertDialog.Builder(MainActivity.this);

              // Atribui as propriedades do dialogo
              dialogo.setTitle("Diálogo");
              dialogo.setMessage("Apresentando um Diálogo com uma mensagem simples!!!");
              dialogo.setNeutralButton("OK", null);

              // Apresenta o dialogo na tela
              dialogo.show();
            }
        });
    }
}
